CC=gcc
CFLAGS= -D_XOPEN_SOURCE=700 -D_FORTIFY_SOURCE=2 -std=c99 -Wall -Wextra -pedantic -Os -fstack-protector-strong -lrt
CPPCHKFLAGS= --std=posix --std=c99 --platform=unix64 --enable=all
SPARSEFLAGS= -Wsparse-all
SCANBUILDFLAGS= -enable-checker core -enable-checker security -enable-checker unix
SCANBUILD=scan-build 

do:VQ: twc twc.1
  : this section intentionally left blank

%: %.c
  $SCANBUILD $CC $CFLAGS $stem.c -o $stem
  sparse $SPARSEFLAGS $target.c
  cppcheck $CPPCHKFLAGS $target.c

clean:V:
  rm -f twc twc.1
