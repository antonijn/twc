#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/* If it's good enough for Pike and co it's good enough for us */
#define uchar unsigned char
#define ulong unsigned long
#define ASCIIMAX UCHAR_MAX+1
#define BUFSIZE 1024


#define HELPSTRING "twc -- tiny word count\n" \
                   "Usage: twc OPTION <files ...>\n" \
                   "Options:\n" \
                   "\t-c, --bytes\n\t\t\tPrint the character/byte count\n" \
                   "\t-w, --words\n\t\t\tPrint the word count\n" \
                   "\t-l, --lines\n\t\t\tPrint the line count\n"

enum equclasses_e {
	EQ_END,
	EQ_BYTE,
	EQ_NEWLINE,
	EQ_WSPACE,
	EQ_NUMCLASSES
};

enum states_e {
	S_END,
	S_BYTE,
	S_ISWORD,
	S_SPACE,
	S_NUMSTATES
};

enum results_e {
	RESULTS_BYTES = 0x01,
	RESULTS_WORDS = 0x02,
	RESULTS_LINES = 0x04,
	RESULTS_ALL   = 0x07,
	RESULTS_HELP  = 0x08
};

typedef struct {
	ulong bytes, words, newlines;
} file_stats;


static int equclass[ASCIIMAX] = { 0 };
static int states[S_NUMSTATES][EQ_NUMCLASSES] = { { 0 } };

static int count_newlines[EQ_NUMCLASSES] = { 0 };
static int count_bytes[S_NUMSTATES]      = { 0 };
static int count_words[S_NUMSTATES]      = { 0 };


static void init_state_machine(void)
{
	/* Set up equivalence classes */
	size_t i = 0;
	for (i = 0; i < ASCIIMAX; i++) {
		equclass[i] = EQ_BYTE;
	}

	equclass['\0'] = EQ_END;
	equclass['\n'] = EQ_NEWLINE;

	/* Treating \r as whitespace is a bit of a hack, yes.
	 * We *can* create more complex machinary to cope with it but,
	 * it's really outside the scope of this, to be quite honest */
	equclass[' '] = equclass['\r'] = equclass['\t'] = EQ_WSPACE;

	/* Translate equivalence classes to state transitions */
	/* S_END is also S_START, which has some nice properties, but you
	 * probably wouldn't want to do this with more complex machines */
	states[S_END][EQ_END]     = S_END;
	states[S_END][EQ_BYTE]    = S_BYTE;
	states[S_END][EQ_NEWLINE] = S_SPACE;
	states[S_END][EQ_WSPACE]  = S_SPACE;

	states[S_BYTE][EQ_END]     = S_END;
	states[S_BYTE][EQ_BYTE]    = S_BYTE;
	states[S_BYTE][EQ_NEWLINE] = S_ISWORD;
	states[S_BYTE][EQ_WSPACE]  = S_ISWORD;

	states[S_ISWORD][EQ_END]     = S_END;
	states[S_ISWORD][EQ_BYTE]    = S_BYTE;
	states[S_ISWORD][EQ_NEWLINE] = S_SPACE;
	states[S_ISWORD][EQ_WSPACE]  = S_SPACE;
	
	states[S_SPACE][EQ_END]     = S_END;
	states[S_SPACE][EQ_BYTE]    = S_BYTE;
	states[S_SPACE][EQ_NEWLINE] = S_SPACE;
	states[S_SPACE][EQ_WSPACE]  = S_SPACE;


	count_newlines[EQ_NEWLINE] = 1;

	count_words[S_ISWORD] = 1;

	count_bytes[S_BYTE] = count_bytes[S_ISWORD] = count_bytes[S_SPACE] = 1;
}


static int summarize_file(FILE *f, file_stats *stats)
{
	int state = 0;

	uchar buffer[BUFSIZE];
	int i;
	size_t bsize;

	if (!stats) { return 0; }

	stats->bytes = stats->newlines = stats->words = 0;

	while ((bsize = fread(buffer, 1, BUFSIZE, f)) > 0) {
		i = 0;
		do {
			int equ = equclass[buffer[i++]];
			state = states[state][equ];

			stats->newlines += count_newlines[equ];
			stats->bytes    += count_bytes[state];
			stats->words    += count_words[state];
		} while (i < bsize && state != S_END);
	}

	return 1;
}


static ssize_t opt(char *arg, int *opt_flag)
{
	ssize_t i;

	if (!arg) { return -1; }

	/* Long option */
	if (arg[1] == '-') {
		if (strcmp(arg, "--help") == 0) { *opt_flag |= RESULTS_HELP; }
		else if (strcmp(arg, "--bytes") == 0) { *opt_flag |= RESULTS_BYTES; }
		else if (strcmp(arg, "--words") == 0) { *opt_flag |= RESULTS_WORDS; }
		else if (strcmp(arg, "--lines") == 0) { *opt_flag |= RESULTS_LINES; }
		else { return -1; }

		return 0;
	}

	/* Single-letter options */
	for (i = 1; arg[i]; i++) {
		switch (arg[i]) {
		case 'h': *opt_flag |= RESULTS_HELP; break;
		case 'c': *opt_flag |= RESULTS_BYTES; break;
		case 'w': *opt_flag |= RESULTS_WORDS; break;
		case 'l': *opt_flag |= RESULTS_LINES; break;
		default: return -1;
		}
	}

	return 0;
}

static ssize_t opts(char **argv, size_t *fileix, int *opt_flag)
{
	size_t i = 1;
	ssize_t n = 0;

	*opt_flag = 0;

	if (!argv[i] || strlen(argv[i]) == 0) { return -1; }

	for (i = 1; argv[i] && argv[i][0] == '-'; i++)
		if (opt(argv[i], opt_flag) < 0) { return -1; }

	/* Nothing specified means output everything */
	if ((*opt_flag & RESULTS_ALL) == 0) { *opt_flag |= RESULTS_ALL; }

	*fileix = i;
	
	for (; argv[i]; i++)
		n++;

	return n;
}


/* Number of base-10 digits of n */
static int numdigs(ulong n)
{
	int res;
	for (res = 1; n /= 10; res++)
		;
	return res;
}

static void print_file_stats(int optflag, char *statname, file_stats *stats,
                             file_stats *colwidths)
{
	if (!statname) { return; }

	if (optflag & RESULTS_LINES) {
		printf("  %*lu", colwidths->newlines, stats->newlines);
	}
	if (optflag & RESULTS_WORDS) {
		printf("  %*lu", colwidths->words, stats->words);
	}
	if (optflag & RESULTS_BYTES) {
		printf(" %*lu", colwidths->bytes, stats->bytes);
	}
	printf(" %s\n", statname);
}


int main(int argc, char **argv)
{
	file_stats *stats = NULL;
	file_stats *total = NULL;
	file_stats colwidths = { 0 };
	ssize_t num_stats = 0;
	size_t filearg_index = 0;
	int optflags = 0;

	ssize_t i = 0;

	if (argc == 1 || (num_stats = opts(argv, &filearg_index, &optflags)) < 0) {
		fprintf(stderr, "Bad / Insufficient arguments.\n");
		fprintf(stderr, HELPSTRING);
		goto _throw;
	}
	if (optflags & RESULTS_HELP) {
		printf(HELPSTRING);
		return 0;
	}
	if (num_stats < 1 || !filearg_index) {
		fprintf(stderr, "No files provided!\n");
		fprintf(stderr, HELPSTRING);
		goto _throw;
	}

	if (!(stats = calloc(num_stats + 1, sizeof(file_stats)))) {
		perror("calloc");
		goto _throw;
	}

	total = &stats[num_stats];

	init_state_machine();

	for (i = 0; i < num_stats; i++) {
		FILE *f;
		int arg_index = filearg_index + i;

		f = fopen(argv[arg_index], "rb");
		if (!f) { perror(argv[0]); goto _throw; }
		if (!summarize_file(f, &stats[i])) { goto _throw; }
		fclose(f);
		
		total->bytes += stats[i].bytes;
		total->words += stats[i].words;
		total->newlines += stats[i].newlines;
	}

	colwidths.bytes = numdigs(total->bytes);
	colwidths.words = numdigs(total->words);
	colwidths.newlines = numdigs(total->newlines);

	for (i = 0; i < num_stats; i++) {
		print_file_stats(optflags, argv[i+filearg_index], &stats[i], &colwidths);
	}

	if (num_stats > 1) {
		print_file_stats(optflags, "total", total, &colwidths);
	}

	free(stats);
	return 0;
_throw:
	free(stats);
	return 1;
}
