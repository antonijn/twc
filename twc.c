#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define uchar unsigned char
#define ulong unsigned long
#define ASCIIMAX 256

#define HELPSTRING "twc -- tiny word count\n" \
                   "Usage: twc OPTION <files ...>\n" \
                   "Options:\n" \
                   "\t-c, --bytes\n\t\t\tPrint the character/byte count\n" \
                   "\t-w, --words\n\t\t\tPrint the word count\n" \
                   "\t-l, --lines\n\t\t\tPrint the line count\n"

enum { EQ_END, EQ_BYTE, EQ_NEWLINE, EQ_WSPACE, EQ_NCLASSES };
enum { S_END, S_BYTE, S_ISWORD, S_SPACE, S_NSTATES };

struct fstats {
	uchar *s;
	ulong bytes, words, newlines;
};

int equclass[ASCIIMAX];
int states[S_NSTATES][EQ_NCLASSES];

int cntnl[EQ_NCLASSES];
int cntbt[S_NSTATES];
int cntwd[S_NSTATES];

enum { R_BYTES = 0x01, R_WORDS = 0x02, R_LINES = 0x04, R_ALL = 0x07, R_HELP = 0x08 };

static uchar *readbuf(char *path)
{
	struct stat st;
	uchar *s = NULL;
	int fd = -1;
	size_t size = 0;

	/* get data about file (including size in bytes) */
	if ((stat(path, &st)) < 0) { perror(path); goto _throw; }
	
	size = st.st_size;
	if (size == 0) { printf("%s is empty.\n", path); goto _throw; }

	/* The size of a char is always one */
	s = calloc(size, 1);
	if (!s) { perror("calloc"); goto _throw; }

	fd = open(path, O_RDONLY);
	if (fd < 0) { perror(path); goto _throw; }
	if ((read(fd, s, size)) < 0) { perror("read"); goto _throw; }
	close(fd); /* There's no point checking close for errors */
	
	return s;

_throw:
	if (fd > 0) { close(fd); }
	if (s) free(s);
	return NULL;
}


static int init_sm(void)
{
	/* Set up equivalence classes */
	size_t i = 0;
	for (i = 0; i < ASCIIMAX; i++)
		equclass[i] = EQ_BYTE;
	equclass['\0'] = EQ_END;
	equclass['\n'] = EQ_NEWLINE;
	equclass[' '] = equclass['\r'] = equclass['\t'] = EQ_WSPACE;

	/* Set up state automata */
	states[S_END][EQ_END] = S_END;
	states[S_END][EQ_BYTE] = S_BYTE;
	states[S_END][EQ_NEWLINE] = S_SPACE;
	states[S_END][EQ_WSPACE] = S_SPACE;

	states[S_BYTE][EQ_END] = S_END;
	states[S_BYTE][EQ_BYTE] = S_BYTE;
	states[S_BYTE][EQ_NEWLINE] = S_ISWORD;
	states[S_BYTE][EQ_WSPACE] = S_ISWORD;

	states[S_ISWORD][EQ_END] = S_END;
	states[S_ISWORD][EQ_BYTE] = S_BYTE;
	states[S_ISWORD][EQ_NEWLINE] = S_SPACE;
	states[S_ISWORD][EQ_WSPACE] = S_SPACE;
	
	states[S_SPACE][EQ_END] = S_END;
	states[S_SPACE][EQ_BYTE] = S_BYTE;
	states[S_SPACE][EQ_NEWLINE] = S_SPACE;
	states[S_SPACE][EQ_WSPACE] = S_SPACE;


	cntnl[EQ_BYTE] = 0; cntnl[EQ_WSPACE] = 0; cntnl[EQ_NEWLINE] = 1;
	cntbt[S_BYTE] = 1; cntbt[S_ISWORD] = 1; cntbt[S_SPACE] = 1;
	cntwd[S_BYTE] = 0; cntwd[S_ISWORD] = 1; cntwd[S_SPACE] = 0;
	return 1;
}


static int count_file(struct fstats *stats)
{
	uchar *s = NULL;
	int state;

	if (!stats || !stats->s) { return 0; }
	s = stats->s;
	stats->bytes = stats->newlines = stats->words = state = 0;
	do {
		int equ = equclass[*s++];
		state = states[state][equ];

		stats->newlines += cntnl[equ];
		stats->bytes += cntbt[state];
		stats->words += cntwd[state];
	} while (state > S_END);
	return 1;
}


static ssize_t opt(char *arg, int *opt_flag)
{
	ssize_t i;

	if (!arg) { return -1; }

	/* Long option */
	if (arg[1] == '-') {
		if (strcmp(arg, "--help") == 0) { *opt_flag |= R_HELP; }
		else if (strcmp(arg, "--bytes") == 0) { *opt_flag |= R_BYTES; }
		else if (strcmp(arg, "--words") == 0) { *opt_flag |= R_WORDS; }
		else if (strcmp(arg, "--lines") == 0) { *opt_flag |= R_LINES; }
		else { return -1; }

		return 0;
	}

	/* Single-letter options */
	for (i = 1; arg[i]; i++) {
		switch (arg[i]) {
		case 'h': *opt_flag |= R_HELP; break;
		case 'c': *opt_flag |= R_BYTES; break;
		case 'w': *opt_flag |= R_WORDS; break;
		case 'l': *opt_flag |= R_LINES; break;
		default: return -1;
		}
	}

	return 0;
}

static ssize_t opts(char **argv, size_t *fileix, int *opt_flag)
{
	size_t i = 1;
	ssize_t n = 0;

	*opt_flag = 0;

	if (!argv[i] || strlen(argv[i]) == 0) { return -1; }

	for (i = 1; argv[i] && argv[i][0] == '-'; i++)
		if (opt(argv[i], opt_flag) < 0) { return -1; }

	/* Nothing specified means output everything */
	if ((*opt_flag & R_ALL) == 0) { *opt_flag |= R_ALL; }

	*fileix = i;
	
	for (; argv[i]; i++)
		n++;

	return n;
}


/* Number of base-10 digits of n */
static int numdigs(ulong n)
{
	int res;
	for (res = 1; n /= 10; res++)
		;
	return res;
}

static void calc_alignment(int optflag, struct fstats *stats, ssize_t nstats,
                           struct fstats *colwidths)
{
	ssize_t i;

	ulong max_bytes = 0, max_words = 0, max_lines = 0;

	for (i = 0; i < nstats; ++i) {
		if (stats[i].bytes > max_bytes) { max_bytes = stats[i].bytes; }
		if (stats[i].words > max_words) { max_words = stats[i].words; }
		if (stats[i].newlines > max_lines) { max_lines = stats[i].newlines; }
	}

	colwidths->bytes = numdigs(max_bytes);
	colwidths->words = numdigs(max_words);
	colwidths->newlines = numdigs(max_lines);
}


static void print_fstat(int optflag, char *statname, struct fstats *stats,
                        struct fstats *colwidths)
{
	if (!statname) { return; }

	if (optflag & R_LINES) {
		printf("  %*lu", colwidths->newlines, stats->newlines);
	}
	if (optflag & R_WORDS) {
		printf("  %*lu", colwidths->words, stats->words);
	}
	if (optflag & R_BYTES) {
		printf("  %*lu", colwidths->bytes, stats->bytes);
	}
	printf(" %s\n", statname);
}


static void clean_stats(struct fstats *stats, size_t nstats)
{
	if (stats && nstats > 0) {
		size_t i = 0;
		for (i = 0; i < nstats; i++) {
			if (stats[i].s) free(stats[i].s);
		}
		free(stats);
	}
}
	

int main(int argc, char **argv)
{
	struct fstats *stats = NULL;
	struct fstats *total;
	struct fstats colwidths;
	ssize_t nstats = 0; size_t filesix = 0;
	int opt_flags = 0;
	ssize_t i = 0;

	if (argc == 1 || (nstats = opts(argv, &filesix, &opt_flags)) < 0) {
		fprintf(stderr, "Bad / Insufficient arguments.\n");
		fprintf(stderr, HELPSTRING);
		goto _throw;
	}
	if (opt_flags & R_HELP) { printf(HELPSTRING); return 0; }
	if (nstats < 1 || !filesix) { fprintf(stderr, "Fatal error\n"); goto _throw; }

	stats = calloc(nstats + 1, sizeof(struct fstats));
	if (!stats) { perror("calloc"); goto _throw; }

	total = &stats[nstats];

	init_sm();

	for (i = 0; i < nstats; i++) {
		if (!(stats[i].s = readbuf(argv[i+filesix]))) { goto _throw; }
		if (!count_file(&stats[i])) { goto _throw; }
		total->bytes += stats[i].bytes;
		total->words += stats[i].words;
		total->newlines += stats[i].newlines;
	}

	/* nstats + 1 so we include the total in the alignment */
	calc_alignment(opt_flags, stats, nstats + 1, &colwidths);

	for (i = 0; i < nstats; i++)
		print_fstat(opt_flags, argv[i+filesix], &stats[i], &colwidths);

	if (nstats > 1) { print_fstat(opt_flags, "total", total, &colwidths); }
	clean_stats(stats, nstats + 1);
	return 0;

_throw:
	clean_stats(stats, nstats + 1);
	return 1;
}
